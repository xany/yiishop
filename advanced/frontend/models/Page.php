<?php

namespace frontend\models;

use Yii;
use frontend\models;

/**
 * This is the model class for table "page".
 *
 * @property string $id
 * @property string $title
 * @property string $content
 * @property string $date_created
 * @property string $tag
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $user_id
 *
 * @property PageTopic[] $pageTopics
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    protected $topic_id;
    public static function tableName()
    {
        return 'page';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['date_created', 'user_id'], 'required'],
            [['date_created'], 'safe'],
            [['user_id'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['tag'], 'string', 'max' => 40],
            [['meta_description'], 'string', 'max' => 50],
            [['meta_keywords'], 'string', 'max' => 30],
            [['topic_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'content' => 'Содержимое',
            'date_created' => 'Опубликовано',
            'tag' => 'Теги',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'user_id' => 'Автор ID',
            'topic_id'=>'Тематика'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(Topic::className(), ['id' => 'topic_id'])->viaTable('page_topic', ['page_id'=>'id']);
    }
    public function setTopic_id($id)
    {
        $this->topic_id=$id;
    }

    public function getTopic_id()
    {
        return $this->topic_id;
    }
/*    public function afterSave($insert, $changedAttributes){
        self::setAttribute('topic_id', $this->topic_id);
        parent::afterSave($insert, $changedAttributes);
    }*/
}
