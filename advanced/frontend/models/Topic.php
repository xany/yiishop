<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "topic".
 *
 * @property string $id
 * @property string $topic_name
 *
 * @property PageTopic[] $pageTopics
 */
class Topic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_name'], 'required'],
            [['topic_name'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'topic_name' => 'Topic Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasMany(Page::className(), ['id' => 'page_id'])->viaTable('page_topic', ['topic_id'=>'id']);
    }
}
